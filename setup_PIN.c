#include </home/pi/Desktop/Project_2/define.h>
#include </home/pi/Desktop/Project_2/pulse_R.c>
#include </home/pi/Desktop/Project_2/pulse_L.c>
#include </home/pi/Desktop/Project_2/pulse_H.c>

void setup_PIN(void)
{
    wiringPiSetupGpio();
 
    pinMode(EN1, INPUT);
    pinMode(EN2, INPUT);
    pinMode(EN3, INPUT);
    pinMode(EN4, INPUT);
    pinMode(EN5, INPUT);
    pinMode(EN6, INPUT);
    pinMode(ECHO, INPUT);
 
    pinMode(OUT1, OUTPUT);
    pinMode(OUT2, OUTPUT);
    pinMode(OUT3, OUTPUT);
    pinMode(OUT4, OUTPUT);
    pinMode(OUT5, OUTPUT);
    pinMode(OUT6, OUTPUT);
    pinMode(TRIG, OUTPUT);
 
    softPwmCreate(OUT1, 0, 160);
    softPwmCreate(OUT2, 0, 160);
    softPwmCreate(OUT3, 0, 160);
    softPwmCreate(OUT4, 0, 160);
    softPwmCreate(OUT5, 0, 160);
    softPwmCreate(OUT6, 0, 160);
 
    wiringPiISR(EN1, INT_EDGE_RISING, Pulse_L);
    wiringPiISR(EN3, INT_EDGE_RISING, Pulse_R);
    wiringPiISR(EN5, INT_EDGE_RISING, Pulse_H);
 
    //TRIG pin must start LOW
        digitalWrite(TRIG, LOW);
        delay(30);
}
