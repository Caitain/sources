#ifdef __quay_am_L_T_H__
#else
 #define __quay_am_L_T_H__
 
void quay_am_L_T (int speed)
{
	PID_L(speed, 1.2, 1, 0.001);
	int b = -Output;
	softPwmWrite(OUT1, 0);
	softPwmWrite(OUT2, b);
}

#endif