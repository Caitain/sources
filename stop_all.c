#ifdef __stop_all_H__
#else
 #define __stop_all_H__
 
void stopAll()
{
	softPwmWrite(OUT1,0);
	softPwmWrite(OUT2,0);
	softPwmWrite(OUT3,0);
	softPwmWrite(OUT4,0);
	softPwmWrite(OUT5,0);
	softPwmWrite(OUT6,0);
}

#endif