#include <stdio.h>
#include <stdlib.h>
#include <wiringPi.h>
#include <softPwm.h>

#ifdef __define_H__
#else 
 #define __define_H__

 //Encoder L BCM
 #define EN1 17
 #define EN2 18
 
 //Encoder R
 #define EN3 27
 #define EN4 22 
 
 //Encoder H
 #define EN5 23
 #define EN6 24
 
//dc L
 #define OUT1    20
 #define OUT2    21
 
 //dc R
 #define OUT3    19
 #define OUT4    26
 
 //dc H
 #define OUT5    13
 #define OUT6    16
 
 //SR-04
 #define ECHO    25
 #define TRIG    12
 
 volatile int PulseL = 0, PulseR = 0, PulseH = 0;
 volatile int Error = 0, Derivator = 0, Integrator = 0;
 volatile int P_value = 0, I_value = 0, D_value = 0;
 volatile int Integrator_max = 500, Integrator_min = -500;
 volatile int Output;
#endif
