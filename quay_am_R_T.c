#ifdef __quay_am_R_T_H__
#else
 #define __quay_am_R_T_H__
 
void quay_am_R_T (int speed)
{
	PID_R(speed, 1.2, 1, 0.001);
	int b = -Output;
	softPwmWrite(OUT3, 0);
	softPwmWrite(OUT4, b);
}

#endif