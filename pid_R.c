#include </home/pi/Desktop/Project_2/define.h>

#ifdef __pid_R_H__
#else
 #define __pid_R_H__

void PID_R (int speed, float Kp, float Ki, float Kd)
{
    Error = speed - PulseR;
   
    P_value = Kp*Error;
    D_value = Kd*(Error - Derivator);
    Derivator = Error;
 
    Integrator = Integrator + Error;
   
    if(Integrator > Integrator_max)
    {
	Integrator = Integrator_max;
    }
    if(Integrator < Integrator_min)
    {
	Integrator = Integrator_min;
    }
 
    I_value = Ki*Integrator;
 
    Output = P_value + I_value + D_value;
}
#endif
